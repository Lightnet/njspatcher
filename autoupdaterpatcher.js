/*
    Information: Working the on the basic launcher for the update and patch.
    Using the nodewebkit and nodejs to act as stand alone patch.    
*/

var config = require('./config.js');
var patchconfig;

var fs = require('fs');
var request = require('request');
//console.log(config);

function Request_Download(_url,_filename,callback){

    var uploadedBytes = 0 ;
    var downloadedBytes = 0;
    var filesizeBytes = 0;
    console.log("request download!");

    var downloadURL = _url;

    var _r = request(downloadURL);
    _r.pipe(fs.createWriteStream(_filename));

    _r.on('response', function(response) {
        filesizeBytes = response.headers['content-length'];
        //console.log(filesizeBytes);
    });

    _r.on('data', function(data) {
      //console.log("data...");
      downloadedBytes += data.length; //download byte total
      //javascript from html
      set_progressbar( Math.floor( (downloadedBytes/filesizeBytes)*100) );
      //set_progressbar(_valeu)
    });
    
    _r.on('end', function () {
        //console.log("finish.");
        callback('finish end');
        //r(downloadURL).pipe(fs.createWriteStream('test.txt')).on('close');
    });    
}

function Request_Update_Version(){
    $('#btn_checkupdate').prop('disabled', true);
    var uploadedBytes = 0 ;
    var downloadedBytes = 0;
    var filesizeBytes = 0;
    //console.log("download...");
    var downloadURL = 'https://bitbucket.org/Lightnet/njspatcher/downloads/patch.json';
    
    Request_Download(downloadURL,'patch.json',function (data){
        console.log('data:>' + data);
        console.log('finish callback');
        $('#btn_checkupdate').prop('disabled', false);
        Check_Update();
    });
}

function Check_Update(){
    patchconfig = require('./patch.json');
    console.log(patchconfig);
    //console.log(patchconfig);
    
    if(patchconfig.CurrentVersion == config.version){
        console.log(config.version + " [=] " +  patchconfig.CurrentVersion );
        console.log("Current Version Match");
        $('#btn_play').show();//html

    }else{
        $('#btn_patch').show();//html
        console.log(config.version + " [=]" +  patchconfig.CurrentVersion );
        console.log("Current Version Does not match it need update.");    
    }
}

function Download_Patch(){
    $('#btn_patch').prop('disabled', true);
    if(patchconfig){
        console.log("patch downloading...");
        console.log(patchconfig.UrlPath);
        
        Request_Download(patchconfig.UrlPath,'patch.rar',function (data){
            console.log('data:>' + data);
            console.log('finish callback');
            $('#btn_patch').prop('disabled', false);
            Check_Update();
        });
    }else{
        console.log("there no patch!");
    }
}

function LanuchSomething(){
    console.log("test launch...");
    window.location = "./index.html"
}






